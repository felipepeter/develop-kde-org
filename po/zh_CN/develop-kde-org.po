msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-05 12:19+0000\n"
"PO-Revision-Date: 2024-03-09 04:50\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/documentation-develop-kde-org/develop-"
"kde-org.pot\n"
"X-Crowdin-File-ID: 25506\n"

#: config.yaml:0
msgid "API"
msgstr "API"

#: i18n/en.yaml:0
msgid "Previous"
msgstr "上一页"

#: i18n/en.yaml:0
msgid "Next"
msgstr "下一页"

#: i18n/en.yaml:0
msgid "Read more"
msgstr "了解更多"

#: i18n/en.yaml:0
msgid "Search this site…"
msgstr "站内搜索..."

#: i18n/en.yaml:0
msgid "in"
msgstr "in"

#: i18n/en.yaml:0
msgid "All Rights Reserved"
msgstr "版权所有"

#: i18n/en.yaml:0
msgid "Privacy Policy"
msgstr "隐私政策"

#: i18n/en.yaml:0
msgid "By"
msgstr "作者"

#: i18n/en.yaml:0
msgid "Created"
msgstr "创建时间"

#: i18n/en.yaml:0
msgid "Last modified"
msgstr "最后修改于"

#: i18n/en.yaml:0
msgid "Edit this page"
msgstr "编辑此页面"

#: i18n/en.yaml:0
msgid "Create documentation issue"
msgstr "创建文档议题"

#: i18n/en.yaml:0
msgid "Create project issue"
msgstr "创建项目议题"

#: i18n/en.yaml:0
msgid "Posts in"
msgstr "发布于"

#: i18n/en.yaml:0
msgid "Icons"
msgstr "图标"

#: i18n/en.yaml:0
msgid "Categories"
msgstr "分类"

#: i18n/en.yaml:0
msgid "Search for icons"
msgstr "搜索图标"

#: i18n/en.yaml:0
msgid "Start typing to filter..."
msgstr "开始过滤..."

#: i18n/en.yaml:0
msgid "Nothing found for this category/keyword combination."
msgstr "找不到此类别/关键字组合。"

#: i18n/en.yaml:0
msgid "KDE Developer Platform"
msgstr "KDE 开发者平台"

#: i18n/en.yaml:0
msgid ""
"Design, build and distribute beautiful, usable applications with KDE "
"technologies."
msgstr "使用 KDE 技术设计、构建、分发美丽而实用的应用程序。"

#: i18n/en.yaml:0
msgid "Multi-Platform"
msgstr "多平台"

#: i18n/en.yaml:0
msgid ""
"Develop once, deploy everywhere. Built on top of Qt, KDE's technologies work "
"on every platform."
msgstr ""
"只需开发一次，即可在任何地方部署。基于 Qt，KDE 的技术在任何平台上都能正常使"
"用。"

#: i18n/en.yaml:0
msgid "Desktop Linux, Android, Windows, macOS, embedded, and more"
msgstr "桌面 Linux、Android、Windows、macOS、嵌入式系统，还有更多"

#: i18n/en.yaml:0
msgid "Plasma running on a phone, laptop and TV"
msgstr "Plasma 在手机、 笔记本电脑和电视上运行"

#: i18n/en.yaml:0
msgid "KDE Frameworks: Enhance the Qt Experience"
msgstr "KDE 框架：提升 Qt 体验"

#: i18n/en.yaml:0
msgid ""
"KDE Frameworks cover 80 add-on libraries for programming with Qt. All "
"libraries have been well-tested in real world scenarios and are "
"comprehensively documented. KDE's libraries are distributed under LGPL or "
"MIT licenses."
msgstr ""
"KDE 框架涵盖了用于使用 Qt 编程的 80 个附加组件库。 所有库都在真实的环境中经过"
"了充分的测试，并且都有全面的文件记录。KDE 的库均在 LGPL 或 MIT 许可证下分发。"

#: i18n/en.yaml:0
msgid "Kirigami UI Framework"
msgstr "Kirigami UI 框架"

#: i18n/en.yaml:0
msgid "Kirigami"
msgstr "Kirigami"

#: i18n/en.yaml:0
msgid ""
"Build Beautiful, Convergent Apps that Run on Phones, TVs and Everything in "
"Between."
msgstr "在移动设备、电视等平台上构建美丽的综合性应用。"

#: i18n/en.yaml:0
msgid ""
"The line between desktop and mobile is blurring and users expect the same "
"quality experience on every device. Applications using Kirigami adapt "
"brilliantly to mobile, desktop, TVs, infortainment systems and everything in "
"between."
msgstr ""
"桌面和移动设备之间的界线正在淡化，用户期望每台设备都有同样的高质量体验。 使"
"用 Kirigami 的应用能够很好地适应移动设备、台式计算机、电视、车载多媒体系统等"
"平台。"

#: i18n/en.yaml:0
msgid ""
"Kirigami's components are goodlooking and consistent, and Kirigami itself "
"provides a clearly defined workflow. Users of Kirigami apps will appreciate "
"the smart choices made in the API and uncluttered design."
msgstr ""
"Kirigami 的部件美观而协调，并且其本身提供了明确界定的工作流程。 Kirigami 应用"
"的用户会感谢开发者在 API 和无缝设计中做出的聪明选择。"

#: i18n/en.yaml:0
msgid "Discover Kirigami"
msgstr "探索 Kirigami"

#: i18n/en.yaml:0
msgid "KDevelop"
msgstr "KDevelop"

#: i18n/en.yaml:0
msgid "A cross-platform IDE for C, C++, Python, QML/JavaScript and PHP"
msgstr "一款支持 C、C++、Python、QML/JavaScript 和 PHP 的跨平台 IDE 程序。"

#: i18n/en.yaml:0
msgid ""
"Open Source, powerful and fast, KDevelop  offers a seamless development "
"environment to programmers that work on projects of any size. KDevelop helps "
"you get the job done while staying out of your way."
msgstr ""
"开源，强大和快速，KDevelop 为从事任何规模项目的程序员提供了一个无缝衔接的开发"
"环境。 KDevelop 可以帮助您完成任务，同时不影响您的操作。"

#: i18n/en.yaml:0
msgid "Get KDevelop"
msgstr "获取 KDevelop"

#: i18n/en.yaml:0
msgid "Documentation"
msgstr "文档"

#: i18n/en.yaml:0
msgid ""
"From beginners to experienced Qt developers, here is all you will need to "
"know to start developing KDE applications."
msgstr ""
"从初学者到经验丰富的 Qt 开发者，这里有您需要知道的开始开发 KDE 应用程序的一切"
"知识。"

#: i18n/en.yaml:0
msgid "Design"
msgstr "设计"

#: i18n/en.yaml:0
msgid "Learn about KDE's design guidelines for apps and icons"
msgstr "学习 KDE 的应用程序和图标设计指南"

#: i18n/en.yaml:0
msgid "Develop"
msgstr "开发"

#: i18n/en.yaml:0
msgid "Check out the tools and libraries that help you build KDE apps"
msgstr "查看帮助您构建 KDE 应用程序的工具和库"

#: i18n/en.yaml:0
msgid "Distribute"
msgstr "分发"

#: i18n/en.yaml:0
msgid "Get your application in front of users"
msgstr "将您的应用程序呈现在用户面前"
